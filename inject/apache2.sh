#!/bin/bash -x

source /host/settings.sh

### install apache2
apt-get -y install apache2

### setup the configuration for schooltool
### read more at http://book.schooltool.org/apache.html
cat <<EOF > /etc/apache2/sites-available/schooltool.conf
<VirtualHost *:80>
    ServerName $DOMAIN
    RedirectPermanent / https://$DOMAIN/
</VirtualHost>

<VirtualHost *:443>
    ServerName $DOMAIN
    ProxyPass / http://127.0.0.1:7080/++vh++https:$DOMAIN:443/++/

    SSLEngine on
    SSLOptions +FakeBasicAuth +ExportCertData +StrictRequire
    SSLCertificateFile        /etc/ssl/certs/ssl-cert-snakeoil.pem
    SSLCertificateKeyFile   /etc/ssl/private/ssl-cert-snakeoil.key
    #SSLCertificateChainFile  /etc/ssl/certs/ssl-cert-snakeoil.pem
</VirtualHost>
EOF

### update config and restart apache2
a2enmod ssl proxy proxy_http rewrite
a2ensite schooltool
a2dissite 000-default
service apache2 restart

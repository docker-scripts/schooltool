schooltool
==========

Docker scripts that install and run SchoolTool in a container.

## Install

  - First install `ds` and `revproxy`:
     + https://gitlab.com/docker-scripts/ds#installation
     + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the schooltool scripts: `ds pull schooltool`

  - Create a directory for the schooltool container: `ds init schooltool @school1`

  - Fix the settings:
    ```
    cd /var/ds/school1
    vim settings.sh
    ds info
    ```

  - Build image, create the container and configure it:
    ```
    ds build
    ds create
    ds config
    ```

## Other commands

```
ds shell
ds stop
ds start
ds snapshot

ds backup
ds restore

ds help
```
